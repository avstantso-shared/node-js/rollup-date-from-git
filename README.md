# rollup-date-from-git
This is the Rollup plugin. It's replace specified functions calls to new Date with data from Git

## Example

Declare function for replace (in this case `sitemap.ts`):
```
export function rollupLastmodFromGit(...relPaths: string[]): Date {
  return undefined;
}
```
  
Source for replace (in this case `home-info.ts`):
```
import Pages from './pages';
import { rollupLastmodFromGit } from './sitemap';

export const HomePageInfo = {
  path: '/',
  element: <Pages.home />,
  sitemapMeta: {
    lastmod: rollupLastmodFromGit('./pages/home'),
    },
  }
```

Rollup config (partial):
```
import rollupDateFromGit from '@avstantso/node-js--rollup-date-from-git';
...
export default [
  {
    ...
    plugins: [
      rollupDateFromGit({
        exclude: './src/sitemap.tsx',
        funcs: 'rollupLastmodFromGit',
      }),
      ...
    ],
    ...
  },
]
```

Source after replace:
```
import Pages from './pages';

export const HomePageInfo = {
  path: '/',
  element: <Pages.home />,
  sitemapMeta: {
    lastmod: new Date(1689002128000),
    },
  }
```

`1689002128000` is result of
```
new Date(
  `${execSync(
    `git log -1 --pretty="format:%ci" "${fullPath}"`,
    {
      shell: true,
    }
  )}`
).valueOf();
```
where
```
fullPath = path.resolve(<path to home-info.ts>, '..', './pages/home')
```  
`'./pages/home'` — `rollupLastmodFromGit` function argument (it must be a literal!).
If there are multiple arguments, the largest date is taken.