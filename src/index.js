const path = require('path');
const fs = require('fs');
const { execSync } = require('child_process');

const { createFilter } = require('rollup-pluginutils');

function dateFromGit(fullPath, firstDate) {
  const s = `${execSync(
    `git log -1 --pretty="format:%ci"${
      firstDate ? ' --diff-filter=A --follow --' : ''
    } "${fullPath}"`,
    {
      shell: true,
    }
  )}`;

  return s ? new Date(s) : new Date();
}

function rollupDateFromGit(opts = {}) {
  if (!opts.funcs) {
    throw Error(`"funcs" option: (string | string[]) should be specified`);
  }

  const filter = createFilter(
    opts.include || ['**/*.ts', '**/*.tsx'],
    opts.exclude
  );

  // Try add to path if it not found
  const extensions = opts.extensions || [
    '.tsx',
    '.ts',
    '.json',
    '.jsx',
    '.js',
    '.htm',
    '.html',
  ];

  function tryDateFromGit(fullPath, firstDate) {
    const existsExts =
      ['', ...extensions].find((ext) => fs.existsSync(`${fullPath}${ext}`)) ||
      '';
    return dateFromGit(`${fullPath}${existsExts}`, firstDate);
  }

  return {
    name: 'rollup-date-from-git',

    transform(code, id) {
      if (!filter(id)) return;

      function modifyCode(regExp, replacer = '') {
        const c = code.replace(regExp, replacer);
        return !!(c !== code && (code = c));
      }

      (Array.isArray(opts.funcs) ? opts.funcs : [opts.funcs]).forEach(
        (func) => {
          const { name: funcName, firstDate } =
            'string' === typeof func ? { name: func, firstDate: false } : func;

          const existsRegEx = new RegExp(
            `(^|(?<=[^\\w\\d]))${funcName}($|(?=[^\\w\\d]))`,
            'g'
          );

          if (!existsRegEx.test(code)) return;

          const importOnlyRegEx = new RegExp(
            `import\\s*\\{\\s*${funcName}\\s*\\}\\s*from\\s*'[^']*';\\r?\\n?`,
            'gm'
          );
          if (!modifyCode(importOnlyRegEx)) {
            const importInList = new RegExp(
              `import\\s*\\{.*[^\\w\\d]${funcName}\\s*(,[^\\}]+)?\\}\\s*from\\s*'[^']*';\\r?\\n?`,
              'gm'
            );

            modifyCode(importInList, (match) =>
              match.replace(
                new RegExp(`(?<=[^\\w\\d])${funcName}\\s*,?\\s*`, 'gm'),
                ''
              )
            );
          }

          const callRegExp = new RegExp(
            `(^|(?<=[^\\w\\d]))${funcName}\\((\\s*'[^']*',?)+\\s*\\)`,
            'gm'
          );

          code = code.replace(callRegExp, (match) => {
            const paths = [...match.matchAll(/'[^']*'/g)].map((p) =>
              p[0].substring(1, p[0].length - 1)
            );

            const dates = paths
              .map((p) => tryDateFromGit(path.resolve(id, '..', p), firstDate))
              .sort((a, b) => a.valueOf() - b.valueOf());

            return `new Date(${dates[0].valueOf()})`;
          });
        }
      );

      return {
        code,
        map: { mappings: '' },
      };
    },
  };
}

module.exports = rollupDateFromGit;
